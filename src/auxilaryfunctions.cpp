#include "include/auxilaryfunctions.h"

#include <QChar>
#include <QString>

int getRandomNumber(int min, int max) {
  return qrand() % (max - min + 1) + min;
}

QChar getRandomLetter(QString str) {
  const int randomIndex = getRandomNumber(0, str.length() - 1);
  return str.at(randomIndex);
}
