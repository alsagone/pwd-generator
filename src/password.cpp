#include "include/password.h"

#include <QChar>
#include <QList>
#include <QString>
#include <QtDebug>

#include "include/auxilaryfunctions.h"

Password::Password(int length, bool boolAlphanumeric, bool boolUniqueChars) {
  this->symbols = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
  this->digits = "0123456789";
  this->lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
  this->uppercaseLetters = lowercaseLetters.toUpper();
  this->desiredLength = length;
  this->alphanumeric = boolAlphanumeric;
  this->uniqueChars = boolUniqueChars;
  this->password = "";
}

charType Password::getCharType(QChar c) {
  charType type = charType::undefined;

  if (Password::symbols.contains(c)) {
    type = charType::symbol;
  }

  else if (c.isDigit()) {
    type = charType::digit;
  }

  else if (lowercaseLetters.contains(c)) {
    type = charType::lowercaseLetter;
  }

  else if (uppercaseLetters.contains(c)) {
    type = charType::uppercaseLetter;
  }

  else {
    qDebug() << "Error: unknown character '" << c << "'\n";
  }

  return type;
}

QList<charType> Password::getAvailableTypes(charType forbiddenType) {
  QList<charType> availableTypes;

  if ((forbiddenType != charType::uppercaseLetter) &&
      (uppercaseLetters.length() > 0)) {
    availableTypes.append(charType::uppercaseLetter);
  }

  if ((forbiddenType != charType::lowercaseLetter) &&
      (lowercaseLetters.length() > 0)) {
    availableTypes.append(charType::lowercaseLetter);
  }

  if ((forbiddenType != charType::digit) && (digits.length() > 0)) {
    availableTypes.append(charType::digit);
  }

  if ((!Password::alphanumeric) && (forbiddenType != charType::symbol) &&
      (symbols.length() > 0)) {
    availableTypes.append(charType::symbol);
  }

  return availableTypes;
}

charType Password::getRandomType(QList<charType> availableTypes) {
  const int randomIndex = getRandomNumber(0, availableTypes.size() - 1);
  return availableTypes.at(randomIndex);
}

void Password::removeChar(QChar c, charType type) {
  switch (type) {
    case charType::uppercaseLetter:
      uppercaseLetters.remove(c);
      break;

    case charType::lowercaseLetter:
      lowercaseLetters.remove(c);
      break;

    case charType::digit:
      digits.remove(c);
      break;

    case charType::symbol:
      symbols.remove(c);
      break;

    default:
      break;
  }
}

void Password::buildPassword() {
  charType forbiddenType = charType::undefined;
  charType randomType;
  QList<charType> availableTypes;
  QChar randomLetter;

  for (int i = 1; i < Password::desiredLength; i++) {
    availableTypes = Password::getAvailableTypes(forbiddenType);
    randomType = Password::getRandomType(availableTypes);

    switch (randomType) {
      case charType::uppercaseLetter:
        randomLetter = getRandomLetter(uppercaseLetters);
        break;

      case charType::lowercaseLetter:
        randomLetter = getRandomLetter(lowercaseLetters);
        break;

      case charType::digit:
        randomLetter = getRandomLetter(digits);
        break;

      case charType::symbol:
        randomLetter = getRandomLetter(symbols);
        break;

      default:
        break;
    }

    if (Password::uniqueChars) {
      Password::removeChar(randomLetter, randomType);
    }

    this->password.append(randomLetter);
    forbiddenType = randomType;
  }
}
