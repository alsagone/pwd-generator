#include "include/mainwindow.h"

#include <QApplication>
#include <QClipboard>
#include <QDebug>
#include <QStringList>

#include "include/auxilaryfunctions.h"
#include "include/password.h"
#include "include/ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  MainWindow::passwordList = new QStringList();
  listModel = new QStringListModel(*MainWindow::passwordList, NULL);
  ui->listeMotsDePasse->setModel(listModel);
  ui->listeMotsDePasse->setSelectionMode(QAbstractItemView::SingleSelection);
  listModel->setStringList(*MainWindow::passwordList);

  MainWindow::clipboard = QApplication::clipboard();
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_boutonLongueurAleatoire_clicked() {
  int randomLength = getRandomNumber(15, 96);
  MainWindow::length = randomLength;
  ui->longueur->setValue(randomLength);
}

void MainWindow::on_boutonGenerer_clicked() {
  MainWindow::length = ui->longueur->value();
  MainWindow::uniqueChars = (ui->checkboxCaracteresUniques->isChecked());
  MainWindow::alphanumeric = (!ui->checkboxSymboles->isChecked());

  Password p(MainWindow::length, MainWindow::alphanumeric,
             MainWindow::uniqueChars);

  p.buildPassword();
  MainWindow::passwordList->append(p.password);
  listModel->setStringList(*MainWindow::passwordList);
  return;
}

void MainWindow::on_longueur_valueChanged(int newValue) {
  if (newValue > 96) {
    ui->checkboxCaracteresUniques->setChecked(false);
    ui->checkboxCaracteresUniques->setCheckable(false);
    ui->checkboxCaracteresUniques->setDisabled(true);
  }

  return;
}

void MainWindow::on_boutonViderListe_clicked() {
  MainWindow::passwordList->clear();
  listModel->setStringList(*MainWindow::passwordList);
}

void MainWindow::on_boutonCopier_clicked() {
  QModelIndex selectedValueIndex = ui->listeMotsDePasse->currentIndex();
  QString selectedPassword =
      selectedValueIndex.data(Qt::DisplayRole).toString();

  MainWindow::clipboard->setText(selectedPassword, QClipboard::Clipboard);
}
