#ifndef PASSWORD_H
#define PASSWORD_H
#pragma once
#include <QChar>
#include <QList>
#include <QString>

#include "include/auxilaryfunctions.h"

class Password {
 public:
  Password(int length, bool boolAlphanumeric, bool boolUniqueChars);
  charType getCharType(QChar c);
  QList<charType> getAvailableTypes(charType forbiddenType);
  charType getRandomType(QList<charType> availableTypes);
  void removeChar(QChar c, charType type);
  void buildPassword();

  int desiredLength;
  bool alphanumeric;
  bool uniqueChars;

  QString symbols;
  QString digits;
  QString lowercaseLetters;
  QString uppercaseLetters;
  QString password;
};
#endif // PASSWORD_H
