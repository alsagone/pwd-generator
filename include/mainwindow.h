#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QClipboard>
#include <QMainWindow>
#include <QStringList>
#include <QStringListModel>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  int length;
  bool alphanumeric;
  bool uniqueChars;
  QStringList *passwordList;
  QStringListModel *listModel;
  QClipboard *clipboard;

 private slots:
  void on_boutonLongueurAleatoire_clicked();
  void on_boutonGenerer_clicked();

  void on_longueur_valueChanged(int newValue);

  void on_boutonViderListe_clicked();

  void on_boutonCopier_clicked();

 private:
  Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
