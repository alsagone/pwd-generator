#ifndef AUXILARYFUNCTIONS_H
#define AUXILARYFUNCTIONS_H
#include <QChar>
#include <QString>

int getRandomNumber(int min, int max);
QChar getRandomLetter(QString str);
enum charType : int {
  symbol,
  digit,
  lowercaseLetter,
  uppercaseLetter,
  undefined
};

#endif // AUXILARYFUNCTIONS_H
