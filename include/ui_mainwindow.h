/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow {
 public:
  QWidget *centralwidget;
  QWidget *verticalLayoutWidget_4;
  QVBoxLayout *verticalLayout_5;
  QLabel *label_2;
  QHBoxLayout *horizontalLayout_3;
  QVBoxLayout *verticalLayout;
  QVBoxLayout *verticalLayout_3;
  QHBoxLayout *horizontalLayout_2;
  QLabel *label;
  QSpinBox *longueur;
  QPushButton *boutonLongueurAleatoire;
  QCheckBox *checkboxSymboles;
  QCheckBox *checkboxCaracteresUniques;
  QPushButton *boutonGenerer;
  QSpacerItem *verticalSpacer;
  QHBoxLayout *horizontalLayout;
  QVBoxLayout *verticalLayout_2;
  QLabel *label_3;
  QListView *listeMotsDePasse;
  QSpacerItem *verticalSpacer_2;
  QVBoxLayout *verticalLayout_4;
  QPushButton *boutonCopier;
  QPushButton *boutonViderListe;
  QMenuBar *menubar;

  void setupUi(QMainWindow *MainWindow) {
    if (MainWindow->objectName().isEmpty())
      MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
    MainWindow->resize(763, 334);
    QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
    MainWindow->setSizePolicy(sizePolicy);
    MainWindow->setMinimumSize(QSize(763, 334));
    MainWindow->setMaximumSize(QSize(763, 334));
    centralwidget = new QWidget(MainWindow);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
    verticalLayoutWidget_4 = new QWidget(centralwidget);
    verticalLayoutWidget_4->setObjectName(
        QString::fromUtf8("verticalLayoutWidget_4"));
    verticalLayoutWidget_4->setGeometry(QRect(40, 20, 691, 271));
    verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_4);
    verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
    verticalLayout_5->setContentsMargins(0, 0, 0, 0);
    label_2 = new QLabel(verticalLayoutWidget_4);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    QFont font;
    font.setPointSize(20);
    label_2->setFont(font);
    label_2->setAlignment(Qt::AlignCenter);

    verticalLayout_5->addWidget(label_2);

    horizontalLayout_3 = new QHBoxLayout();
    horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    verticalLayout_3 = new QVBoxLayout();
    verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    label = new QLabel(verticalLayoutWidget_4);
    label->setObjectName(QString::fromUtf8("label"));

    horizontalLayout_2->addWidget(label);

    longueur = new QSpinBox(verticalLayoutWidget_4);
    longueur->setObjectName(QString::fromUtf8("longueur"));
    longueur->setMinimum(15);

    horizontalLayout_2->addWidget(longueur);

    verticalLayout_3->addLayout(horizontalLayout_2);

    boutonLongueurAleatoire = new QPushButton(verticalLayoutWidget_4);
    boutonLongueurAleatoire->setObjectName(
        QString::fromUtf8("boutonLongueurAleatoire"));

    verticalLayout_3->addWidget(boutonLongueurAleatoire);

    verticalLayout->addLayout(verticalLayout_3);

    checkboxSymboles = new QCheckBox(verticalLayoutWidget_4);
    checkboxSymboles->setObjectName(QString::fromUtf8("checkboxSymboles"));
    checkboxSymboles->setChecked(true);

    verticalLayout->addWidget(checkboxSymboles);

    checkboxCaracteresUniques = new QCheckBox(verticalLayoutWidget_4);
    checkboxCaracteresUniques->setObjectName(
        QString::fromUtf8("checkboxCaracteresUniques"));
    checkboxCaracteresUniques->setChecked(true);

    verticalLayout->addWidget(checkboxCaracteresUniques);

    boutonGenerer = new QPushButton(verticalLayoutWidget_4);
    boutonGenerer->setObjectName(QString::fromUtf8("boutonGenerer"));

    verticalLayout->addWidget(boutonGenerer);

    horizontalLayout_3->addLayout(verticalLayout);

    verticalSpacer =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    horizontalLayout_3->addItem(verticalSpacer);

    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    verticalLayout_2 = new QVBoxLayout();
    verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
    label_3 = new QLabel(verticalLayoutWidget_4);
    label_3->setObjectName(QString::fromUtf8("label_3"));

    verticalLayout_2->addWidget(label_3);

    listeMotsDePasse = new QListView(verticalLayoutWidget_4);
    listeMotsDePasse->setObjectName(QString::fromUtf8("listeMotsDePasse"));

    verticalLayout_2->addWidget(listeMotsDePasse);

    horizontalLayout->addLayout(verticalLayout_2);

    verticalSpacer_2 =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    horizontalLayout->addItem(verticalSpacer_2);

    verticalLayout_4 = new QVBoxLayout();
    verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
    boutonCopier = new QPushButton(verticalLayoutWidget_4);
    boutonCopier->setObjectName(QString::fromUtf8("boutonCopier"));

    verticalLayout_4->addWidget(boutonCopier);

    boutonViderListe = new QPushButton(verticalLayoutWidget_4);
    boutonViderListe->setObjectName(QString::fromUtf8("boutonViderListe"));

    verticalLayout_4->addWidget(boutonViderListe);

    horizontalLayout->addLayout(verticalLayout_4);

    horizontalLayout_3->addLayout(horizontalLayout);

    verticalLayout_5->addLayout(horizontalLayout_3);

    MainWindow->setCentralWidget(centralwidget);
    menubar = new QMenuBar(MainWindow);
    menubar->setObjectName(QString::fromUtf8("menubar"));
    menubar->setGeometry(QRect(0, 0, 763, 20));
    MainWindow->setMenuBar(menubar);

    retranslateUi(MainWindow);

    QMetaObject::connectSlotsByName(MainWindow);
  }  // setupUi

  void retranslateUi(QMainWindow *MainWindow) {
    MainWindow->setWindowTitle(QCoreApplication::translate(
        "MainWindow", "G\303\251n\303\251rateur de mot de passe", nullptr));
    label_2->setText(QCoreApplication::translate(
        "MainWindow", "G\303\251n\303\251rateur de mot de passe", nullptr));
    label->setText(
        QCoreApplication::translate("MainWindow", "Longueur", nullptr));
    boutonLongueurAleatoire->setText(QCoreApplication::translate(
        "MainWindow", "Longueur al\303\251atoire", nullptr));
    checkboxSymboles->setText(
        QCoreApplication::translate("MainWindow", "Symboles", nullptr));
    checkboxCaracteresUniques->setText(QCoreApplication::translate(
        "MainWindow", "Caract\303\250res uniques", nullptr));
    boutonGenerer->setText(QCoreApplication::translate(
        "MainWindow", "G\303\251n\303\251rer", nullptr));
    label_3->setText(QCoreApplication::translate(
        "MainWindow", "Mots de passe g\303\251n\303\251r\303\251s", nullptr));
    boutonCopier->setText(
        QCoreApplication::translate("MainWindow", "Copier", nullptr));
    boutonViderListe->setText(
        QCoreApplication::translate("MainWindow", "Vider la liste", nullptr));
  }  // retranslateUi
};

namespace Ui {
class MainWindow : public Ui_MainWindow {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_MAINWINDOW_H
